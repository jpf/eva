package com.eva.biz.system;

import com.eva.dao.system.dto.CreateRoleMenuDTO;
import com.eva.dao.system.dto.CreateRolePermissionDTO;
import com.eva.dao.system.model.SystemRole;

import java.util.List;

/**
 * 角色权限业务处理
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public interface SystemRoleBiz {

    /**
     * 新建
     *
     * @param systemRole 角色
     * @return Integer
     */
    Integer create (SystemRole systemRole);

    /**
     * 修改
     *
     * @param systemRole 角色
     */
    void updateById(SystemRole systemRole);

    /**
     * 删除
     *
     * @param id 角色ID
     */
    void deleteById(Integer id);

    /**
     * 批量删除
     *
     * @param ids 角色ID列表
     */
    void deleteByIdInBatch(List<Integer> ids);

    /**
     * 创建角色权限
     *
     * @param dto 详见CreateRolePermissionDTO
     */
    void createRolePermission(CreateRolePermissionDTO dto);

    /**
     * 创建角色菜单
     *
     * @param dto 详见CreateRoleMenuDTO
     */
    void createRoleMenu(CreateRoleMenuDTO dto);
}
