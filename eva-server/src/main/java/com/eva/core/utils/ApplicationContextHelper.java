package com.eva.core.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Spring上下文工具
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
@Component
public class ApplicationContextHelper {

    @Autowired
    public ApplicationContext applicationContext;

    /**
     * 获取Bean实例
     *
     * @param name 类注册名称
     * @return Object
     */
    public Object getBean(String name) {
        return applicationContext.getBean(name);
    }

    /**
     * 获取Bean实例
     *
     * @param clazz Class
     * @return T
     */
    public <T> T getBean(Class<T> clazz){
        return applicationContext.getBean(clazz);
    }

    /**
     * 获取Bean实例
     *
     * @param name 类注册名称
     * @param clazz Class
     * @return T
     */
    public <T> T getBean(String name, Class<T> clazz) {
        return applicationContext.getBean(name, clazz);
    }
}
