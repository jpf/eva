package com.eva.dao.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eva.dao.system.dto.QuerySystemPermissionDTO;
import com.eva.dao.system.model.SystemPermission;
import com.eva.dao.system.vo.SystemPermissionListVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemPermissionMapper extends BaseMapper<SystemPermission> {

    /**
     * 根据用户ID查询
     *
     * @param userId 用户ID
     * @return List<SystemPermission>
     */
    List<SystemPermission> selectByUserId(Integer userId);

    /**
     * 根据角色ID查询
     *
     * @param roleId 角色ID
     * @return List<SystemPermission>
     */
    List<SystemPermission> selectByRoleId(Integer roleId);

    /**
     * 查询权限管理列表
     *
     * @return List<SystemPermissionListVO>
     */
    List<SystemPermissionListVO> selectManageList();

}
