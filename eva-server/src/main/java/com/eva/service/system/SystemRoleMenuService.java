package com.eva.service.system;

import com.eva.core.model.PageData;
import com.eva.core.model.PageWrap;
import com.eva.dao.system.model.SystemRoleMenu;
import java.util.List;

/**
 * 角色菜单关联Service定义
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public interface SystemRoleMenuService {

    /**
     * 创建
     *
     * @param systemRoleMenu 实体
     * @return Integer
     */
    Integer create(SystemRoleMenu systemRoleMenu);

    /**
     * 主键删除
     *
     * @param id 主键
     */
    void deleteById(Integer id);

    /**
     * 删除
     *
     * @param systemRoleMenu 删除条件
     */
    void delete(SystemRoleMenu systemRoleMenu);

    /**
     * 批量主键删除
     *
     * @param ids 主键列表
     */
    void deleteByIdInBatch(List<Integer> ids);

    /**
     * 主键更新
     *
     * @param systemRoleMenu 主体
     */
    void updateById(SystemRoleMenu systemRoleMenu);

    /**
     * 批量主键更新
     *
     * @param systemRoleMenus 主体列表
     */
    void updateByIdInBatch(List<SystemRoleMenu> systemRoleMenus);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return SystemRoleMenu
     */
    SystemRoleMenu findById(Integer id);

    /**
     * 条件查询单条记录
     *
     * @param systemRoleMenu 查询条件
     * @return SystemRoleMenu
     */
    SystemRoleMenu findOne(SystemRoleMenu systemRoleMenu);

    /**
     * 条件查询
     *
     * @param systemRoleMenu 查询条件
     * @return List<SystemRoleMenu>
     */
    List<SystemRoleMenu> findList(SystemRoleMenu systemRoleMenu);

    /**
     * 分页查询
     *
     * @param pageWrap 分页参数
     * @return PageData<SystemRoleMenu>
     */
    PageData<SystemRoleMenu> findPage(PageWrap<SystemRoleMenu> pageWrap);

    /**
     * 条件统计
     *
     * @param systemRoleMenu 统计条件
     * @return long
     */
    long count(SystemRoleMenu systemRoleMenu);
}
