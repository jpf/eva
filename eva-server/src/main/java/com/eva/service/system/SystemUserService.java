package com.eva.service.system;

import com.eva.core.model.PageData;
import com.eva.core.model.PageWrap;
import com.eva.dao.system.dto.QuerySystemUserDTO;
import com.eva.dao.system.model.SystemUser;
import com.eva.dao.system.vo.SystemUserListVO;

import java.util.List;

/**
 * 系统用户Service定义
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public interface SystemUserService {

    /**
     * 创建
     *
     * @param systemUser 实体
     * @return Integer
     */
    Integer create(SystemUser systemUser);

    /**
     * 主键删除
     *
     * @param id 主键
     */
    void deleteById(Integer id);

    /**
     * 批量主键删除
     *
     * @param ids 主键列表
     */
    void deleteByIdInBatch(List<Integer> ids);

    /**
     * 主键更新
     *
     * @param systemUser 实体
     */
    void updateById(SystemUser systemUser);

    /**
     * 批量主键更新
     *
     * @param systemUsers 实体列表
     */
    void updateByIdInBatch(List<SystemUser> systemUsers);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return SystemUser
     */
    SystemUser findById(Integer id);

    /**
     * 条件查询单条记录
     *
     * @param systemUser 查询条件
     * @return SystemUser
     */
    SystemUser findOne(SystemUser systemUser);

    /**
     * 条件查询
     *
     * @param systemUser 查询条件
     * @return List<SystemUser>
     */
    List<SystemUser> findList(SystemUser systemUser);

    /**
     * 分页查询
     *
     * @param pageWrap 分页参数
     * @return PageData<SystemUserListVO>
     */
    PageData<SystemUserListVO> findPage(PageWrap<QuerySystemUserDTO> pageWrap);

    /**
     * 条件统计
     *
     * @param systemUser 统计参数
     * @return long
     */
    long count(SystemUser systemUser);
}
