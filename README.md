# 介绍

**伊娃（Eva）** 是一款基于SpringBoot、Mybatis Plus的，支持在线定制化技术栈，支持在线定制化功能模块的开源的后台管理系统项目框架。跟其他开源项目一样，为了使项目开发足够简单，Eva经过仔细的设计和探索，采用**约定大于配置的思想**，基于**可扩展性、安全性、可读性**对项目的常用功能进行了合理的封装，并在文档中进行了详细的介绍和说明。

# 立即使用

GIT上的为基础代码，请前往前往 **[在线定制代码](http://coderd.adjustrd.com/template/308/default)** 获取定制代码，想要了解更多内容，请前往 **[官网](http://eva.adjustrd.com/)**

GIT上的为基础代码，请前往前往 **[在线定制代码](http://coderd.adjustrd.com/template/308/default)** 获取定制代码，想要了解更多内容，请前往 **[官网](http://eva.adjustrd.com/)**

GIT上的为基础代码，请前往前往 **[在线定制代码](http://coderd.adjustrd.com/template/308/default)** 获取定制代码，想要了解更多内容，请前往 **[官网](http://eva.adjustrd.com/)**

# 在线体验

[立即前往伊娃系统](http://online-v3.eva.adjustrd.com/)

你可以使用以下模拟的角色进行登录，不同的角色拥有不同的操作权限！

- 超级管理员：admin/123123
- 普通管理员：manager/123123
- 人事专员：hr/123123
- 普通员工：staff/123123

# 技术选型
**后端**
- SpringBoot 2.x
- MyBatis Plus 3.4.2
- Swagger 2.9.2
- Apache Shiro 1.7/Spring Secure（可选）
- Alibaba Druid 1.2
- 内置缓存/Redis（可选）

**前端**
- Vue 2.x
- Vuex 3.4
- vue-cli 3.0
- Sass 4.12.0
- Element-UI 2.3.6
- Axios 0.21.1
- Eslint 6.7.2


# 联系我们 & 技术支持

您可以扫码加入Eva交流群以了解项目最新进度，并获得项目技术支持！

![/qq.png](/eva-front/src/assets/images/qq.png)
